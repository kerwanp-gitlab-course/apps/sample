# Exercice 2 - Livraison continue

Dans ce deuxième exercice vous allez créer un colis.
Le colis correspond au livrable de notre application.

De cette manière il est facilement possible de télécharger le livrable d'une version spéficique et de lancer l'application.

Un livrable n'est pas forcément seulement le code, ça peut-être une image Docker, une package JS, ou autre.

Dans notre cas nous souhaitons créer une image Docker, nous pourrons ensuite l'utiliser pour la déployer locallement, sur un VPS ou sur un Cluster Kubernetes.

## Consigne

En utilisant la documentation de Gitlab, ajoutez un nouveau job qui effectuera la release de votre application.

Ce job devra :

- Construire notre image grâce au `Dockerfile`
- Attribuer une version à notre image sous le format `v1.0`
- Créer une nouvelle release avec comme description la commande permettant de `pull` notre image
- S'exécuter seulement sur la branch `main`

> ⚠ Attention, créer une release, va aussi créer aussi un tag ! Vous allez donc créer une boucle infinie : le pipeline se lance, créer une release, créer un tag, lance le pipeline, et on recommence. Trouvez la `rules` qui évite ça !

> 📎 Pour automatiquement incrémenter la version, faites un tour dans les variables prédéfinies de GitlabCI.
