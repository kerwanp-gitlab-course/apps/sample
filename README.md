# Sample App

## Available scripts

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn e2e`

Launches Cypress End-to-end testing Framework.
More information on the official documentation: https://docs.cypress.io/guides/overview/why-cypress

### `yarn prettier`

Run Prettier over the project to ensure that every file is properly formatted.

### `yarn eslint`

Run ESLint over the projet to ensure that every file is properly linted.

## Exercices

### <a href="exercices/EXERCICE-1.md">1. Intégration continue</a>

### <a href="exercices/EXERCICE-2.md">2. Livraison continue</a>

### <a href="exercices/EXERCICE-3.md">3. Déploiement continu</a>
