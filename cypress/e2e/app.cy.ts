describe('App', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it('renders the app', () => {
    cy.get('.app').should('exist');
  });

  it('should calculate `5 + 2`', () => {
    cy.get('input[name="inputA"]').type('5');
    cy.get('input[name="inputB"]').type('2');
    cy.get('button').click();
    cy.get('h3').should('contain', '8');
  });

  it('should calculate `5.5 + 3.2`', () => {
    cy.get('input[name="inputA"]').type('5.5');
    cy.get('input[name="inputB"]').type('3.2');
    cy.get('button').click();
    cy.get('h3').should('contain', '8.7');
  });
});
