import { defineConfig } from 'cypress';

export default defineConfig({
  e2e: {
    baseUrl: 'http://localhost:3000',
  },
  reporter: 'junit',
  reporterOptions: {
    mochaFile: 'reports/cypress.xml',
    toConsole: true,
  },
});
