import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import Form from './Form';

const mockSubmit = jest.fn(async (d: number) => await Promise.resolve(d));

describe('Form', () => {
  test('`5 + 2` should be equal to 7', async () => {
    render(<Form onSubmit={mockSubmit} />);

    fireEvent.input(screen.getByRole('textbox', { name: /A/i }), {
      target: {
        value: 5,
      },
    });

    fireEvent.input(screen.getByRole('textbox', { name: 'B' }), {
      target: {
        value: 2,
      },
    });

    fireEvent.submit(screen.getByRole('button'));

    await waitFor(() => {
      expect(mockSubmit).toBeCalled();
    });

    expect(mockSubmit).toBeCalledWith(7);
  });

  test('`2.5 + 3.2` should be equal to 5.7', async () => {
    render(<Form onSubmit={mockSubmit} />);

    fireEvent.input(screen.getByRole('textbox', { name: /A/i }), {
      target: {
        value: 2.5,
      },
    });

    fireEvent.input(screen.getByRole('textbox', { name: 'B' }), {
      target: {
        value: 3.2,
      },
    });

    fireEvent.submit(screen.getByRole('button'));

    await waitFor(() => {
      expect(mockSubmit).toBeCalled();
    });

    expect(mockSubmit).toBeCalledWith(5.7);
  });
});
